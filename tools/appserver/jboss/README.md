JBoss Setup
===========
This project sets up a JBoss 7 Application Server. However, it will only be performed when the setup-jboss profile
is active.

Set it up by running:

    $ mvn initialize -P setup-jboss

The server will be available in target/server/jboss-as-<version>.

## Start the server
Start the server as usual:

    $ target/server/jboss-as-<version>/bin/standalone.sh

## HTTP port
The server will HTTP will run on port 8080. This can be changed by setting the property maven.jboss.port.offset
(which adds an offset to all ports as defined in standalone.xml) when setting up the server.

For example:

    $ mvn clean install -Dmaven.jboss.port.offset=2000

Will make the HTTP port become 10080.